//requirements
var express = require('express');
var logger = require('morgan');
var sync = require("./sync")
//const bodyParser = require('body-parser');



//init
var app = express();
app.use(logger('dev'));
app.use(express.json())
//app.use(bodyParser);
// app
  const router = require('./router');
  app.use('/', router);

//set sync schedule for 1h

setInterval(() => {
  sync.sync();
}, 3600000);

app.listen(8000, function() {
  //console.log('Gateway listening on port 8000!');
});

