//const db= require("./database")
const { DynamoDBClient, PutItemCommand } = require("@aws-sdk/client-dynamodb");
const {fromIni} = require("@aws-sdk/credential-provider-ini");
var fs = require('fs')

const sendState = (req,res) =>{
    
    var csid = req.body.CSID;
    var timestamp = req.body.timestamp;
    var temperature = req.body.temperature;
    var humidity = req.body.humidity;
    var pm10 = req.body.pm10;
    var pm25 = req.body.pm25;
    var co2 = req.body.co2;
    var voc = req.body.voc;
    var validated=true;  
    var csdata=csid+"x"+timestamp;

    if(csid != undefined && (typeof csid == 'number') ){
        var csid=JSON.stringify(csid);
    }else {
        //console.log("csid validation failed");
        validated=false;
    }
    
    if(timestamp != undefined && (typeof timestamp == 'number') ){
        var timestamp=JSON.stringify(timestamp);
    }else {
        //console.log("timestamp validation failed");
        validated=false;
    }

    if(temperature != undefined && (typeof temperature == 'number') ){
        var temperature=JSON.stringify(temperature);
    }else {
        //console.log("temperature validation failed");
        validated=false;
    }

    if(humidity != undefined && (typeof humidity == 'number') ){
        var humidity=JSON.stringify(humidity);
    }else {
        //console.log("humidity validation failed");
        validated=false;
    }

    if(pm10 != undefined && (typeof pm10 == 'number') ){
        var pm10=JSON.stringify(pm10);
    }else {
        //console.log("pm10 validation failed");
        validated=false;
    }

    if(pm25 != undefined && (typeof pm25 == 'number') ){
        var pm25=JSON.stringify(pm25);
    }else {
        //console.log("pm25 validation failed");
        validated=false;
    }

    if(co2 != undefined && (typeof co2 == 'number') ){
        var co2=JSON.stringify(co2);
    }else {
        //console.log("co2 validation failed");
        validated=false;
    }

    if(voc != undefined && (typeof voc == 'number') ){
        var voc=JSON.stringify(voc);
    }else {
        //console.log("voc validation failed");
        validated=false;
    }
    //console.log(req.body);
    //TODO: validate input

    if(validated){

        

        var params = {
            Item: {
             "CSData": {
               "S": csdata
              }, 
             "CSID": {
               'S': csid
              }, 
              
             "timestamp": {
               "N": timestamp
              }, 
              
              "temperature": {
                N: temperature
               }, 
              "humidity": {
                 N: humidity
              }, 
              "pm10": {
                  N: pm10
              }, 
              "pm25": {
                  N: pm25
              }, 
              "co2": {
                  N: co2
              }, 
              "voc": {
                  N: voc
              }
              
            }, 
            ReturnConsumedCapacity: "TOTAL", 
            TableName: "ComfortSensorData"
           };
        fs.readFile('comfortdata.json',  (err, data) => {
            var json = JSON.parse(data);
            //TODO: foreach entry, query DB if is there and remove from json
            json.push(params);
            
            fs.writeFile("comfortdata.json", JSON.stringify(json),(err) => {
                if (err) throw err;
                //console.log('Comfort sensor state has been saved locally!');
            });
            });
        (async () => {
            const client = new DynamoDBClient({ region: "us-east-1", credentials: fromIni({profile: 'default'}) });
            const command = new PutItemCommand(params);
            try {
              const results = await client.send(command);
              //console.log(results.$metadata.httpStatusCode);
              //if()
              res.status(results.$metadata.httpStatusCode).json("Data pushed to DB");
            } catch (err) {
                console.error(err);
                res.status(500).json(" Internal Server Error")
            }
          })();
    } else{
        res.status(400).json("Failed to validate data");
    }
    //db.addComfortSensorState(csid,timestamp,temperature,humidity,pm10,pm25,co2,voc);
    //res.status(501).json("Not implemented yet!")
};

const sendSyncState = (params)=>{
    (async () => {
        const client = new DynamoDBClient({ region: "us-east-1", credentials: fromIni({profile: 'default'}) });
        const command = new PutItemCommand(params);
        try {
          const results = await client.send(command);
          //console.log(results.$metadata.httpStatusCode);
          //if()
          //res.status(results.$metadata.httpStatusCode).json("Data pushed to DB");
        } catch (err) {
            console.error(err);
            //res.status(500).json(" Internal Server Error")
        }
      })();
}

module.exports = {
    sendState: sendState,
    sendSyncState:sendSyncState
  };