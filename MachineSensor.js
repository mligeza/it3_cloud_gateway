//const db= require("./database")
const { DynamoDBClient, PutItemCommand } = require("@aws-sdk/client-dynamodb");
const {fromIni} = require("@aws-sdk/credential-provider-ini");
var fs = require('fs')


const sendState = (req,res) =>{
    //console.log(req.body);
    
    var msid = req.body.MSID;
    var timestamp = req.body.timestamp;
    var temperature = req.body.temperature;
    var radon = req.body.radon;
    var power = req.body.power;
    var msdata=msid+"x"+timestamp;
    var validated=true;    

    if(msid != undefined && (typeof msid == 'number') ){
        var msid=JSON.stringify(msid);
    }else {
        //console.log("MSID validation failed");
        validated=false;
    }

    if(timestamp != undefined && (typeof timestamp == 'number')){
        var timestamp=JSON.stringify(timestamp);
    }else {
        //console.log("timestamp validation failed");
        validated=false;
    }
   
    if(temperature != undefined && (typeof temperature == 'number')){
        var temperature=JSON.stringify(temperature);
    }else {
        //console.log("temperature validation failed");
        validated=false;
    }

    if(radon != undefined && (typeof radon == 'number')){
        var radon=JSON.stringify(radon);
    }else {
        //console.log("radon validation failed");
        validated=false;
    }
    
    if(power != undefined && (typeof power == 'number')){
        var power=JSON.stringify(power);
    }else {
        //console.log("power validation failed");
        validated=false;
    }
 
    
    

    
    if(validated){
        //db.addMachineSensorState(msid,timestamp,temperature,radon,power);
        //console.log("Machine Sensor state validated");
        var params = {
            Item: {
             "MSData": {
               "S": msdata
              }, 
             "MSID": {
               'S': msid
              }, 
              
             "timestamp": {
               "N": timestamp
              }, 
              
              "temperature": {
                N: temperature
               }, 
              "radon": {
                 N: radon
              }, 
              "power": {
                  N: power
              }
              
            }, 
            ReturnConsumedCapacity: "TOTAL", 
            TableName: "MachineSensorData"
           };
           fs.readFile('machinedata.json',  (err, data) => {
            var json = JSON.parse(data);
            //TODO: foreach entry, query DB if is there and remove from json
            json.push(params);
            
            fs.writeFile("machinedata.json", JSON.stringify(json),(err) => {
                if (err) throw err;
                //console.log('Machine sensor state has been saved locally!');
            })
            });
        (async () => {
            const client = new DynamoDBClient({ region: "us-east-1", credentials: fromIni({profile: 'default'}) });
            const command = new PutItemCommand(params);
            try {
              const results = await client.send(command);
              //console.log(results.$metadata.httpStatusCode);
              //if()
              res.status(results.$metadata.httpStatusCode).json("Data pushed to DB");
            } catch (err) {
              console.error(err);
              res.status(500).json(" Internal Server Error")
            }
          })();
        
    } else{
        res.status(400).json("Failed to validate data");
    }
    
    //res.status(501).json("Not implemented yet!")
};

const sendSyncState = (params)=>{
    (async () => {
        const client = new DynamoDBClient({ region: "us-east-1", credentials: fromIni({profile: 'default'}) });
        const command = new PutItemCommand(params);
        try {
          const results = await client.send(command);
          //console.log(results.$metadata.httpStatusCode);
          //if()
          //res.status(results.$metadata.httpStatusCode).json("Data pushed to DB");
        } catch (err) {
            console.error(err);
            //res.status(500).json(" Internal Server Error")
        }
      })();
}

module.exports = {
    sendState: sendState,
    sendSyncState:sendSyncState
  };