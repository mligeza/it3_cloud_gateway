var fs = require('fs')
const comfortSensor= require("./ComfortSensor")
const machineSensor= require("./MachineSensor")
const { DynamoDBClient, GetItemCommand } = require("@aws-sdk/client-dynamodb");
const {fromIni} = require("@aws-sdk/credential-provider-ini");

const sync = ()=> {
    


//--------comfort
                fs.readFile('comfortdata.json', function (err, data) {
                    var json = JSON.parse(data);
                    
                    (async () => {
                        const client = new DynamoDBClient({ region: "us-east-1", credentials: fromIni({profile: 'default'}) });
                        
                        try {
                            
                            var bar = new Promise((resolve, reject) => {
                                json.forEach((element,i,array) => {
                                    ////console.log(element);
                                    ///*
                                    var csdata=element.Item.CSID.S + "x" + element.Item.timestamp.N;
                                    var params = {
                                        Key: {
                                            "CSData": {
                                                "S": csdata
                                            },
                                            "CSID": {
                                                "S": element.Item.CSID.S
                                            }
                                        
                                        },
                                        ReturnConsumedCapacity: "TOTAL", 
                                        TableName: "ComfortSensorData"
                                    };
                                    (async()=> {
                                    const command = new GetItemCommand(params);
                                    const results = await client.send(command);
                                    
                                    ////console.log(results);
                                    if(results.Item === undefined)
                                    {
                                        //add to database
                                        //console.log("c++");
                                        comfortSensor.sendSyncState(element);
                                        
                                        
                                    }
                                    else{
                                        //console.log("c");
                                    }
                                    if (i === array.length -1) resolve();
                                })();
                                   
                                  //*/  
                                  
                                })
                            });
                            bar.then(() => {
                                var empty =[];
                                fs.writeFile("comfortdata.json", JSON.stringify(empty),(err) => {
                                    if (err) throw err;
                                    //console.log('Comfort sensor states were synchronized!');
                                })
                            });
                            
                            
                           
                          } catch (err) {
                              console.error(err);
                              return;
                          }
                          
                        })();
                    //json.push('search result: ' + currentSearchResult)
                    //TODO: foreach entry, query DB if is there and remove from json
                    //const results = await client.send(command);
                   
                    })
//--------machine
fs.readFile('machinedata.json', function (err, data) {
    var json = JSON.parse(data);
    
    (async () => {
        const client = new DynamoDBClient({ region: "us-east-1", credentials: fromIni({profile: 'default'}) });
        
        try {
            var bar2 = new Promise((resolve, reject) => {
                json.forEach((element,i,array) => {
                    ////console.log(element);
                    ///*
                    var msdata=element.Item.MSID.S + "x" + element.Item.timestamp.N;
                    var params = {
                        Key: {
                            "MSData": {
                                "S": msdata
                            },
                            "MSID": {
                                "S": element.Item.MSID.S
                            }
                        
                        },
                        ReturnConsumedCapacity: "TOTAL", 
                        TableName: "MachineSensorData"
                    };
                    (async()=> {
                    const command = new GetItemCommand(params);
                    const results = await client.send(command);
                    
                    ////console.log(results);
                    if(results.Item === undefined)
                    {
                        //add to database
                        //console.log("m++");
                        comfortSensor.sendSyncState(element);
                        
                        //remove from storage
                        const index = json.indexOf(element);
                        json.splice(index, 1);
                    }
                    else{
                        //console.log("m");
                    }
                    if (i === array.length -1) resolve();
                })();
                   
                  //*/  
                });
            });
            
            bar2.then(() => {
                var empty =[];
                fs.writeFile("machinedata.json", JSON.stringify(empty),(err) => {
                    if (err) throw err;
                    //console.log('Machine sensor states were synchronized!');
                })
            });
             
            
           
          } catch (err) {
              console.error(err);
              return;
          }
          
        })();
   
    
    })
             
}

module.exports={
    sync: sync
}