const router = require('express').Router();
const comfortSensor= require("./ComfortSensor")
const machineSensor= require("./MachineSensor")

router.route('/').get((req, res) => {
    res.json("Welcome to gateway!");
});

router.route('/comfort').get((req, res) => {
    res.json("You cannot do anything here now. Please send state via POST /comfort/state!");
});

router.route('/comfort/state').post((req, res) => {
    comfortSensor.sendState(req,res)
    //.then(res.json("State NOT SEND as not implemented"))
    //    .catch(err => res.status(400).json('error: ' + err));
});

router.route('/machine').get((req, res) => {
    res.json("Ypu cannot do anything here now. Please send state via POST /comfort/state!");
});

router.route('/machine/state').post((req, res) => {
    machineSensor.sendState(req,res)
    //.then(res.json("State NOT SEND as not implemented"))
    //    .catch(err => res.status(400).json('error: ' + err));
});

module.exports = router;